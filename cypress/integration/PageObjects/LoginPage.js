class LoginPage {
    visit() {
        cy.visit('https://staging2.getrali.com/')
    }

    login() {
        cy.contains('Login').click()
    }

    fillEmail(value) {
        cy.get('iframe')
            .then(function ($iframe) {
                const $body = $iframe.contents().find('body')
                cy
                    .wrap($body)
                    .find('input:eq(0)')
                    .type(value)    
            })
            return this
    }

    fillPassword(value) {
        cy.get('iframe')
            .then(function ($iframe) {
                const $body = $iframe.contents().find('body')
                cy
                    .wrap($body)
                    .find('input:eq(1)')
                    .type(value)
            })
            return this
    }
    submit() {
        cy.get('iframe')
            .then(function ($iframe) {
                const $body = $iframe.contents().find('body')
                cy
                    .wrap($body)
                    .find('button[type="submit"]')
                    .click()
                    .wait(5000)
            })
    }
}

export default LoginPage