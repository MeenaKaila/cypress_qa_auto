import LoginPage from '../PageObjects/LoginPage'
describe('Login with Page Object Model and custom command', function(){
    it('home page and title', function(){
        const lp = new LoginPage()
        lp.visit()
        cy.title().then(txt => {
            expect(txt).to.eql('Rali - Video at work')
        })
        cy.get('div[data-id="33051be9"]').find('img').should('have.attr', 'src').should('include','RaliLogo')
        cy.contains('Login').should('be.visible') //login button
        cy.contains('Contact Info').should('be.visible')
        cy.contains('Copyright © 2020 Rali, All Rights Reserved.').should('be.visible')
        cy.get('[aria-label="Open Intercom Messenger"]').should('be.visible') //intercom
        
    })
})