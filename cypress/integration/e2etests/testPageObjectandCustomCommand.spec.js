import LoginPage from '../PageObjects/LoginPage'

describe('Login with Page Object Model and custom command', function(){
    it('Login and verify title', function(){
        const lp = new LoginPage()
        lp.visit()
        lp.login()
        lp.fillEmail('meena.kaila@getrali.com')
        lp.fillPassword('Test@123')
        lp.submit()
        cy.title().should('eq', 'Rali: OnQ QA')
        //custom command
        cy.switchToIframe('iframe:eq(0)').find('button:eq(0)').click({ force: true })
        cy.switchToIframe('iframe:eq(0)').contains('Manager').should('exist')
        cy.switchToIframe('iframe:eq(0)').find('div').contains(/^View All$/i).click({ force: true })
        cy.go('back')
        cy.switchToIframe('iframe:eq(0)').find('div').contains(/^View All Series$/i).click({ force: true })
    })
})