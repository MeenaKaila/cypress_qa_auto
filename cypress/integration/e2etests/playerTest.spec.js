describe('Player Test', function () {
    // beforeEach(() => {
    //     cy.on('uncaught:exception', (err) => {
    //       expect(err.message).to.include('Ignoring error for now');
    //       return false;
    //     });
    //   });
    it.only('rali player test with sign in option', function () {
        //var url= 'https://qa.rali.io/player?id=7hmv3ncozc16gnr1q4iw02g6qqmhgrz1'
        //function visit(url) {
        //cy.visit(url, {
        //onBeforeLoad: (win) => {
        //win.onerror = () => {}
        //}
        //});
        //}
        cy.visit('https://qa.rali.io/player?id=1zzagwqzw3ifixm4hxkmlymds031t1kl') //not part of a series
        cy.title().then(txt => {
            expect(txt).to.eql('Autumn leaves')
        })
        cy.get('iframe')
            .then(function ($iFrame) {
                const $body = $iFrame.contents().find('body')
                cy
                    .wrap($body)
                    .find('a').contains('Sign In')
                    .click({ force: true })
            })
        cy.get('iframe')
            .then(function ($iFrame) {
                const $body = $iFrame.contents().find('body')
                cy
                    .wrap($body)
                    .find('input[name="signinEmail"]')
                    .type('meena123@abc.com')
                    .wrap($body)
                    .find('input[name="signinPassword"]')
                    .type('test@123')
                    .wrap($body)
                    .find('div').contains('Sign In')
                    .click({ force: true })
            })
    })
    it('player link', function () {
        //cy.viewport('iphone-8')
        //cy.viewport(1440,900)
        //cy.visit('https://qa.rali.io/player?id=4jbaty00251hiyu98xspztitkzzzl7vb', { timeout: 80000 })
        cy.visit('https://qa.rali.io/player?id=4jbaty00251hiyu98xspztitkzzzl7vb')
        
    })

})