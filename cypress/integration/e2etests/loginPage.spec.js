import LoginPage from '../PageObjects/LoginPage'
describe('Login Page Object Model and custom command', function(){
    it.only('Login page and verify title', function(){
        const lp = new LoginPage()
        lp.visit()
        lp.login()
        cy.title().then(txt => {
            expect(txt).to.eql('Rali: Old onQ QA (default)')
        })
        cy.switchToIframe('iframe').find('img').should('have.attr', 'src').should('include','rali_logo')
        cy.switchToIframe('iframe').contains('Sign up today!').should('be.visible') //sign up
        //cy.scrollTo('bottom')
        cy.switchToIframe('iframe').find('input[name="signinEmail"]').should('have.attr', 'placeholder', 'Enter Email')
        cy.switchToIframe('iframe').find('input[type="password"]').should('have.attr', 'placeholder', 'Enter Password')
        cy.switchToIframe('iframe').find('button[type="submit"]').should('be.disabled')
        //cy.switchToIframe('iframe').find('.flex-wrap.justify-center > button').should('have.length', 4)
        cy.switchToIframe('iframe').find('.flex-wrap.justify-center > button').its('length').as('button')
        cy.get('@button').should('equal', 4) //using alias
        cy.switchToIframe('iframe').contains('Terms and Conditions').should('be.visible')
        cy.switchToIframe('iframe').contains('Privacy Policy').should('exist')
        cy.switchToIframe('iframe').contains('Support').should('exist')
        cy.switchToIframe('iframe').contains('© 2020 Rali').should('exist')
        cy.switchToIframe('iframe').find('div[aria-label="Open Intercom Messenger"]').should('be.visible') //intercom
    })
    it('Forgot password link', function(){
        cy.switchToIframe('iframe').contains('Forgot password?').should('exist').click({ force: true })
        cy.switchToIframe('iframe').contains('Reset your password').should('exist')
        cy.switchToIframe('iframe').find('input[name="email"]').should('have.attr', 'placeholder', 'Enter Email')
        cy.switchToIframe('iframe').find('button[type="button"]').contains('Cancel').should('not.be.disabled')
        cy.switchToIframe('iframe').find('button[type="submit"]').should('be.disabled')
        cy.switchToIframe('iframe').contains('Back to Sign In').should('be.visible')
        cy.switchToIframe('iframe').contains('Back to Sign In').click()
    })
    it('Rali registration page', function(){
        cy.switchToIframe('iframe').contains('Sign up today!').click()
        cy.switchToIframe('iframe').find('img').should('have.attr', 'src').should('include','rali_logo')
        cy.switchToIframe('iframe').contains('Rali Video Registration').should('be.visible')
        cy.switchToIframe('iframe').contains('Sign up for a free Starter Plan.').should('be.visible')
        cy.switchToIframe('iframe').contains('Create User Account').should('be.visible')
        cy.switchToIframe('iframe').find('input[name="signupEmail"]').should('have.attr', 'placeholder', 'Email Address')
        cy.switchToIframe('iframe').contains('max 255 characters').should('exist')
        cy.switchToIframe('iframe').find('input[name="signupFirstName"]').should('have.attr', 'placeholder', 'First Name')
        cy.switchToIframe('iframe').find('.leading-snug:eq(13)').contains('max 50 characters').should('exist')
        cy.switchToIframe('iframe').find('input[name="signupLastName"]').should('have.attr', 'placeholder', 'Last Name')
        cy.switchToIframe('iframe').find('.leading-snug:eq(16)').contains('max 50 characters').should('exist')
        cy.switchToIframe('iframe').find('input[name="signupPassword"]').should('have.attr', 'placeholder', 'Password')
        
    })
})