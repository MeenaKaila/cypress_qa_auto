describe('series landing test', function () {
    it('series landing to player test', function () {
        cy.visit('https://qa.rali.io/series?id=jazaojztks7jxdow3kdar6xbjdzaoyxs')
        // cy.get('iframe')
        //     .then(function ($iFrame) {
        //         const $body = $iFrame.contents().find('body')
        //         cy
        //             .wrap($body)
        //             .find('div').contains(' Brownie recipe')
        //             .click({ force: true })
        //             .wait(5000)
        //     })
        cy.switchToIframe('iframe').contains('Brownie recipe').click({ force: true }).wait(5000)
        cy.title().then(txt => {
            expect(txt).to.eql('Brownie recipe')
        })
    })

    it('click Sign In button on player', function () {
        cy.visit('https://qa.rali.io/player?id=7hmv3ncozc16gnr1q4iw02g6qqmhgrz1')
        cy.switchToIframe('iframe').contains('Sign in').click({ force: true }).wait(5000)
        
        // cy.get('iframe')
        //     .then(function ($iFrame) {
        //         const $body = $iFrame.contents().find('body')
        //         cy
        //             .wrap($body)
        //             .find('div').contains('Sign in')
        //             .click({ force: true })

        //     })
        //     cy.go('back')
    })
    

    it.only('click play button', function () {   
            cy.visit('https://qa.rali.io/player?id=7hmv3ncozc16gnr1q4iw02g6qqmhgrz1') 
            
            cy.get('iframe.flex-grow.embed-view')
                .then(function ($iFrame) {
                    const $body = $iFrame.contents().find('body')
                    cy
                        .wrap($body)
                        //.find('button:eq(15)') //next video
                        .find('button:eq(11)') //play button
                        //.contains('Play') 
                        //.find('div').contains('1 / 3') //cta
                        .click({ force: true })
                        .wait(25000)
                        .wrap($body).find('button:eq(11)').click({ force: true }) 
                        .wait(5000)

            })
    })

    it('click back to series button', function(){
        cy.visit('https://qa.rali.io/player?id=7hmv3ncozc16gnr1q4iw02g6qqmhgrz1') 
        cy.switchToIframe('iframe.flex-grow.embed-view').find('.whitespace-no-wrap').click({ force: true }).wait(5000)
        // cy.get('iframe.flex-grow.embed-view')
        //     .then(function ($iFrame) {
        //         const $body = $iFrame.contents().find('body')
        //         cy
        //             .wrap($body)
        //             //.find('div').contains('Back to Series')
        //             //.find('button[type="button"]').contains('Back to Series')
        //             //.find('.overflow-hidden[type="button"]')
        //             .find(".whitespace-no-wrap")
        //             //.find('button:eq(0)')
        //             .click({ force: true })
        //             .wait(5000)
        //     })
    })

})
