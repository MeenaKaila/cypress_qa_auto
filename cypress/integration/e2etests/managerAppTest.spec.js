//export const START_AUTH = "START_AUTH";

describe('QA Rali Test', function () {
    before(function () {
        cy.visit('https://staging2.getrali.com/');

    });
    it('Login to QA', function () {
        //cy.visit("https://staging2.getrali.com/")
        cy.contains('Login').click()
        cy.get('iframe')
            .then(function ($iframe) {
                const $body = $iframe.contents().find('body')
                cy
                    .wrap($body)
                    .find('input:eq(0)')
                    .type('meena_admin1@understoryonline.com')
                cy
                    .wrap($body)
                    .find('input:eq(1)')
                    .type('password')
                //.wait(5000)
                cy
                    .wrap($body)
                    .find('button[type="submit"]')
                    //.find('div').contains('Sign In')
                    .click()
                    .wait(5000)
                //.click({force: true})                
            })
        //cy.title().should('eq', 'Rali: OnQ QA')
        cy.title().then(txt => {
            expect(txt).to.eql('Rali: OnQ QA')
        })
        cy.get('iframe:eq(0)')
            .then(function ($iFrame) {
                const $body = $iFrame.contents().find('body')
                cy
                    .wrap($body)
                    .find('button:eq(0)') //click on Manager tab
                    .click({ force: true })
                //.find('div.py-1.text-app-white-1').contains('Manager')
                //.find('button:eq(0)').contains('Manager')
                //.find('div').contains('Manager')
                //.find('div.py-1.text-app-white-1:eq(0)').click({force: true})        
            })
        cy.get('iframe:eq(0)')
            .then(function ($iFrame) {
                const $body = $iFrame.contents().find('body')
                cy
                    .wrap($body)
                    //.contains('Manager').should('be.visible')
                    .contains('Manager').should('exist')
                    .wrap($body)
                    //.find('div').contains('Add New Conversation')
                    //.find('div').contains('Add New Series')
                    //.find('div').contains('View All')
                    .find('div').contains(/^View All$/i)
                    .click({ force: true })
                    .wait(5000)
            })

        cy.get('iframe:eq(0)')
            .then(function ($iFrame) {
                const $body = $iFrame.contents().find('body')
                cy
                    .wrap($body)
                    .contains('Conversations').should('be.visible')
                    .wrap($body)
                    .find('div').contains('Published')
                    .click({ force: true })
            })
    })
    it ('click on ManageConversation', function(){
        cy.get('iframe:eq(0)')
        .within(function ($iFrame) {
            const $body = $iFrame.contents().find('body')
            cy
                .wrap($body)
                .contains('Moderate Conversation').should('be.visible')
      //    cy
                .wrap($body)
                .find('button').contains('Manage Conversation')
                .click({ force: true })
                .wait(5000)

        })
    })

    it('testing nestediframe', function(){
        cy.get('iframe:eq(0)').then(($iframe) => {
            const $body = $iframe.contents().find('body')
            cy.wrap($body).find('iframe:eq(0)').then(($iframe2) => {
                const $body = $iframe2.contents().find('body')
                cy.wrap($body)
                    .find('text:eq(0)')
                    .wait(5000)
                    .should('have.text', 'Review Conversation')
                    .wrap($body)
                    .find('button').contains('Creator')
                    .click({ force: true })
            })
        })
    })
    
})