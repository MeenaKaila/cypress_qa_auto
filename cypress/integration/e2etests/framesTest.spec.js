describe('Frame suite', () => {
    it('iFrame - Type inthe body - Javascript Way', () => {
        cy.visit('https://the-internet.herokuapp.com/iframe')
        //cy.get('input[name="Channel Name"]').clear().type('QA Box LET\'s TEST')
        cy.get('iframe#mce_0_ifr').within(fr => {
            const[myIframe]=fr.get()
            myIframe.contentDocument.body.getElementsByTagName('p')[0].textContent= 'hello'
        })
    })
    it('iFrame - Type inthe body - jQuery Way', function ()  {
        cy.visit('https://the-internet.herokuapp.com/iframe')
        
        cy.get('iframe#mce_0_ifr')
            .then(function($frame)  {
                const body = $frame.contents().find('body#tinymce')
                cy.wrap(body).clear().type('Test')

       })
    })

    it('Nested Frames - Fetch the text - Javascript way', () => {
        cy.visit('https://the-internet.herokuapp.com/nested_frames')
        cy.get('frame[src="/frame_top"]').within($frame => {
            const [frame_top] = $frame.get()
            const text = frame_top.contentDocument.body.getElementsByTagName('frame')[1]
                .contentDocument.body.querySelector('div#content').innerText
            expect(text).to.be.eql('MIDDLE')
        })   
       
    })

    it.only('Nested Frames - Fetch the text - jQuery Way', ()=>{
        cy.visit('https://the-internet.herokuapp.com/nested_frames')
        cy.get('frame[src="/frame_top"]').within($frame =>{
            cy.wrap($frame.contents().find('frame[src="/frame_middle"]')).within(fr => {
                cy.wrap(fr.contents().find('div#content')).should('have.text', 'MIDDLE')
            })
        })
    })

})