describe('API testing', function(){
    //read
    it.skip('Get request', function(){
        cy.request('GET', 'https://qa.rali.io/api/platform-svc/secure/user/sendEmailNotification?frequency=WEEKLY&testCalendar=true&moderator=false').then((response) =>{
            expect(response).to.have.property('status', 200)
            expect(response.body).to.not.be.null
        })
    })
    //create
    it('POST request', function(){
        const item = {
            "orgToken": "2c905e1e5c7f0bc5015c7f0c40b9000a",
            "password": "Test@123",
            "username": "meena.kaila@getrali.com"
        }
        cy.request('POST', 'https://qa.rali.io/api/platform-svc/login', item)
        .its('body')
        .should('include',{orgToken:"2c900cde6c4e5968016c4e5a4580000e"})
    })
    //update
    it('PUT request', function(){
        const item = {
            "roleType": "MANAGER"
          }
        cy.request({method:'PUT', url:'https://onq-qa-platform2.herokuapp.com/api/platform-svc/admin/user/2c93c1ae763aeeb601763f27e99f005b/roles?orgUserId=2c93c1ae763aeeb601763f27e99f005b', body:item, failOnStatusCode: false})
        .its('status').should('eq', 401)
    })
})